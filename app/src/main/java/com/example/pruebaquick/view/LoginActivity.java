package com.example.pruebaquick.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pruebaquick.R;
import com.example.pruebaquick.model.data.LocalData;
import com.example.pruebaquick.model.data.entity.Usuario;
import com.example.pruebaquick.utils.FormValidations;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.txtEmail)
    EditText txtEmail;

    @BindView(R.id.txtContrasena)
    EditText txtContrasena;

    private Usuario usuario;
    LocalData localData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        localData = new LocalData(getApplicationContext());
    }


    public void registrarActivity(View v) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    public void ingresar(View v) {
        boolean isValidForm = validarFormulario();
        String contrasena = txtContrasena.getText().toString().trim();
        if (isValidForm) {
            if (contrasena.equals(usuario.getContrasena())) {
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("usuario", usuario);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            } else {
                txtContrasena.setText("");
                txtContrasena.setError("CONTRASEÑA INCORRECTA");
            }
        }
    }

    private boolean validarFormulario() {
        boolean result = true;
        String email = txtEmail.getText().toString().trim();
        String contrasena = txtContrasena.getText().toString().trim();
        FormValidations fv = new FormValidations();
        Usuario usuarioDb = localData.consultarUsuario(email);
        if (fv.isEmpty(email)) {
            result = false;
            txtEmail.setError(fv.CAMPO_REQUERIDO);
        } else if (!fv.isEmail(email)) {
            result = false;
            txtEmail.setError(fv.CAMPO_EMAIL);
        } else if (fv.isEmpty(contrasena)) {
            result = false;
            txtContrasena.setError(fv.CAMPO_CONTRASENA);
        } else if (usuarioDb == null) {
            result = false;
            txtEmail.setError(fv.CAMPO_EMAIL_SIN_REGISTRAR);
        } else {
            usuario = usuarioDb;
        }
        return result;
    }
}
