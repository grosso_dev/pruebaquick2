package com.example.pruebaquick.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.pruebaquick.R;
import com.example.pruebaquick.model.data.entity.Usuario;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    @BindView(R.id.txtUsuario)
    TextView txtUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Bundle usuarioRequest = getIntent().getExtras();
        Usuario usuario = null;
        if(usuarioRequest != null){
            usuario = (Usuario)usuarioRequest.getSerializable("usuario");
            txtUsuario.setText("Bienvenido "+ usuario.getNombreCompleto());
        }
    }
}
