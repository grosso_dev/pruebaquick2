package com.example.pruebaquick.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pruebaquick.R;
import com.example.pruebaquick.model.data.LocalData;
import com.example.pruebaquick.model.data.entity.Usuario;
import com.example.pruebaquick.utils.FormValidations;

;import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.txtNombres)
    EditText txtNombres;

    @BindView(R.id.txtApellidos)
    EditText txtApellidos;

    @BindView(R.id.txtEmail)
    EditText txtEmail;

    @BindView(R.id.txtContrasena)
    EditText txtContrasena;

    @BindView(R.id.txtContrasenaConfirm)
    EditText txtContrasena2;

    private Usuario usuario;
    LocalData localData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        localData = new LocalData(getApplicationContext());
    }


    public void ingresarActivity(View v) {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void registrarUsuario(View v) {
        boolean isValidForm = validarFormulario();
        if (isValidForm) {
            localData.insertarUsuario(usuario);
            ingresarActivity(null);
        }
    }

    private boolean validarFormulario() {
        boolean result = true;
        String nombres = txtNombres.getText().toString().trim();
        String apellidos = txtApellidos.getText().toString().trim();
        String email = txtEmail.getText().toString().trim();
        String contrasena = txtContrasena.getText().toString().trim();
        String contrasena2 = txtContrasena2.getText().toString().trim();
        FormValidations fv = new FormValidations();
        if (fv.isEmpty(nombres)) {
            result = false;
            txtNombres.setError(fv.CAMPO_REQUERIDO);
        } else if (fv.isEmpty(apellidos)) {
            result = false;
            txtApellidos.setError(fv.CAMPO_REQUERIDO);
        } else if (fv.isEmpty(email)) {
            result = false;
            txtEmail.setError(fv.CAMPO_REQUERIDO);
        } else if (!fv.isEmail(email)) {
            result = false;
            txtEmail.setError(fv.CAMPO_EMAIL);
        } else if (!fv.isValidPassword(contrasena)) {
            result = false;
            txtContrasena.setError(fv.CAMPO_CONTRASENA);
        } else if (!contrasena.equals(contrasena2)) {
            result = false;
            txtContrasena2.setError(fv.CAMPO_CONTRASENA_IGUAL);
        } else if (localData.consultarUsuario(email) != null) {
            result = false;
            txtEmail.setError(fv.CAMPO_EMAIL_REGISTRADO);
        } else {
            usuario = new Usuario();
            usuario.setNombres(nombres);
            usuario.setApellidos(apellidos);
            usuario.setContrasena(contrasena);
            usuario.setEmail(email);
        }
        return result;
    }
}