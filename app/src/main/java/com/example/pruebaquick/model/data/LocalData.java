package com.example.pruebaquick.model.data;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.pruebaquick.model.ConexionSqlHelper;
import com.example.pruebaquick.model.data.entity.Usuario;
import com.example.pruebaquick.utils.ConstParametersDB;

public class LocalData {
    ConexionSqlHelper conexionSqlHelper;

    private Context activity;

    public LocalData(Context _activity) {

        this.activity = _activity;
        conexionSqlHelper = new ConexionSqlHelper(this.activity, ConstParametersDB.BASE_DATOS, null, 1);
    }

    public void insertarUsuario(Usuario usuario) {
        SQLiteDatabase db = conexionSqlHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ConstParametersDB.CMP_NOMBRE, usuario.getNombres());
        values.put(ConstParametersDB.CMP_APELLIDO, usuario.getApellidos());
        values.put(ConstParametersDB.CMP_EMAIL, usuario.getEmail());
        values.put(ConstParametersDB.CMP_CONTRASENA, usuario.getContrasena());
        Long idResultante = db.insert(ConstParametersDB.TABLA, "id", values);
        Toast.makeText(this.activity, "Se registro correctamente el usuario", Toast.LENGTH_SHORT).show();
        db.close();
    }

    public Usuario consultarUsuario(String email) {
        Usuario usuario = null;
        SQLiteDatabase db = conexionSqlHelper.getReadableDatabase();
        String[] parametros = {email};
        String[] campos = {
                ConstParametersDB.CMP_EMAIL,
                ConstParametersDB.CMP_CONTRASENA,
                ConstParametersDB.CMP_NOMBRE,
                ConstParametersDB.CMP_APELLIDO
        };
        try {
            Cursor cursor = db.query(ConstParametersDB.TABLA, campos, ConstParametersDB.CMP_EMAIL + "=?", parametros, null, null, null);
            cursor.moveToFirst();
            usuario = new Usuario();
            usuario.setEmail(cursor.getString(0));
            usuario.setContrasena(cursor.getString(1));
            usuario.setNombres(cursor.getString(2));
            usuario.setApellidos(cursor.getString(3));
            cursor.close();
            db.close();
        } catch (Exception e) {
            usuario = null;
        }

        return usuario;
    }
}
