package com.example.pruebaquick.model.data.entity;

import java.io.Serializable;

public class Usuario implements Serializable {
    private String Nombres;
    private String Apellidos;
    private String Email;
    private String Contrasena;

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String contrasena) {
        Contrasena = contrasena;
    }

    public  String getNombreCompleto (){
        return Nombres + " "+ Apellidos;
    }
}

