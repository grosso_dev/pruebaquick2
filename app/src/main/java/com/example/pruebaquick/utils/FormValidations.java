package com.example.pruebaquick.utils;

import android.app.Activity;
import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidations {

    public static final String CAMPO_REQUERIDO = "CAMPO REQUERIDO, DEBE TENER MINIMO 3 CARACTERES";
    public static final String CAMPO_EMAIL = "EL CORREO ES INCORRECTO";
    public static final String CAMPO_CONTRASENA = "LA CONTRASEÑA DEBE TENER MAYUSCULAS, MINUSCULAS NUMEROS Y UN CARACTER";
    public static final String CAMPO_CONTRASENA_IGUAL = "LAS CONTRASEÑAS DEBEN COINCIDIR";
    public static final String CAMPO_EMAIL_REGISTRADO = "EL USUARIO YA SE ENCUENTRA REGISTRADO";
    public static final String CAMPO_EMAIL_SIN_REGISTRAR = "EL USUARIO NO SE ENCUENTRA REGISTRADO";

    public boolean isEmpty(String value) {
        boolean result = false;
        if (value.trim().isEmpty()) {
            result = true;
        } else if (value.length() < 3) {
            result = true;
        }

        return result;
    }

    public boolean isEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    public boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@*#$%^&+=])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

}
