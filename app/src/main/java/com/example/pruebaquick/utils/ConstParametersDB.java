package com.example.pruebaquick.utils;

public class ConstParametersDB {

    //constantes de la estructura de la base de datos
    public static final String BASE_DATOS = "db_prueba";
    public static final String TABLA = "usuario";
    public static final String CMP_NOMBRE = "nombre";
    public static final String CMP_APELLIDO = "apellido";
    public static final String CMP_EMAIL = "email";
    public static final String CMP_CONTRASENA = "contrasena";

    //sql para crear tabla en la base de datos
    public static final String CREAR_TABLA =
            "CREATE TABLE " +
                    TABLA + " ( id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    CMP_NOMBRE + " TEXT," +
                    CMP_APELLIDO + " TEXT," +
                    CMP_EMAIL + " TEXT," +
                    CMP_CONTRASENA + " TEXT )";

    //sql para eliminar tabla en la base de datos
    public static final String ELIMINAR_TABLA = "DROP TABLE IF EXISTS "+ TABLA;
}
